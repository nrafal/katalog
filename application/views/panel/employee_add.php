<div class="container">
    <h1>Zarządzanie katalogami pracowników.</h1>
    <div class="navbar">
      <div class="navbar-inner nav-collapse" style="height: auto;">
        <ul class="nav">
          <li><a href="/kohana/admin/employee">Powróć do poprzedniej strony</a></li>
        </ul>
      </div>
    </div>
    <div class="well">
        <?php if (isset($message)) {
            ?>
            <div style="color: #000000; text-align: center;"><?php echo $message ; ?></div>
            <?php
        }
        ?>
        <?php echo Form::open('admin/employee/add',array('enctype' => 'multipart/form-data')); ?>
        
        <?php echo Form::label('firstname', 'Imię pracownika:'); ?>
        <?php echo Form::input('firstname'); ?>
        
        <?php echo Form::label('lastname', 'Nazwisko pracownika:'); ?>
        <?php echo Form::input('lastname'); ?>
        
        <?php echo Form::label('mail', 'E-mail pracownika:'); ?>
        <?php echo Form::input('mail'); ?>
        
        <?php echo Form::label('image', 'Zdjęcie profilowe:'); ?>
        <?php echo Form::file('image'); ?>
        
        <?php echo Form::label('catalog', 'Należy do katalogu:'); ?>
        <?php echo Form::select('catalog', $catalogs) ; ?>
        
        <br/><br/>
        <?php echo Form::submit('create', 'Utwórz nowego pracownika',array('class' => 'btn btn-success')); ?>
        <?php echo Form::close(); ?>
    </div>
</div>