<div class="container">
    <h1>Zarządzanie katalogami pracowników.</h1>
    <div class="navbar">
      <div class="navbar-inner nav-collapse" style="height: auto;">
        <ul class="nav">
          <li><a href="/kohana/admin/employee/add">Dodaj nowego</a></li>
          <li><a href="/kohana/admin">Powróc do poprzedniej strony</a></li>
        </ul>
      </div>
    </div>
    <div class="well">
        <?php 
            if (is_string($results))
                echo $results ;
            else {
                ?>
                <table cellpadding="0" cellspacing="0" border="0" class="table">
                <?php
                foreach ($results as $result) {
                    ?>
                    <tr>
                        <td><?php echo $result['firstname'] ?></td>
                        <td><?php echo $result['lastname'] ?></td>
                        <td><a href="/kohana/admin/employee/show/<?php echo $result['id'] ?>">Pokaż</a></td>
                        <td><a href="/kohana/admin/employee/delete/<?php echo $result['id'] ?>">Usuń</a></td>
                    </tr>
                    <?php 
                }
                ?>
                </table>
                <?php
            }
        ?>
    </div>
</div>