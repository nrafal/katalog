<div class="container">
    <h1>Zarządzanie katalogami pracowników.</h1>
    <div class="navbar">
      <div class="navbar-inner nav-collapse" style="height: auto;">
        <ul class="nav">
          <li><a href="/kohana/admin/employee">Powróć do poprzedniej strony</a></li>
        </ul>
      </div>
    </div>
    <div class="span4 well">
        <div class="row">
            <div class="span3"><img src="/kohana/<?php echo $result[0]['link'] ;?>"></div>
            <div class="span3">
                <p><strong><?php echo $result[0]['firstname'] . ' ' . $result[0]['lastname'] ?></strong></p>
                <p><i><?php echo $result[0]['mail'] ?></i></p>
                <p><strong>Katalog: </strong><?php echo $result[0]['name'] ?></strong></p>
            </div>
        </div>
    </div>
</div>