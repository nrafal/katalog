<div class="container">
    <h1>Zarządzanie katalogami pracowników.</h1>
    <div class="navbar">
      <div class="navbar-inner nav-collapse" style="height: auto;">
        <ul class="nav">
          <li><a href="/kohana/admin/catalog">Powróć do poprzedniej strony</a></li>
        </ul>
      </div>
    </div>
    <div class="well">
        <?php if (isset($message)) {
            ?>
            <div style="color: #000000; text-align: center;"><?php echo $message ; ?></div>
            <?php
        }
        ?>
        <?php echo Form::open('admin/catalog/edit/' . $result[0]->id); ?>
        
        <?php echo Form::label('name', 'Nazwa katalogu:'); ?>
        <?php echo Form::input('name',$result[0]->name); ?>
        <br/><br/>
        <?php echo Form::submit('changes', 'Zapisz zmiany',array('class' => 'btn btn-success')); ?>
        <?php echo Form::close(); ?>
    </div>
</div>