<div class="container">
    <div class="hero-unit" style="text-align: center;">
        <h1>Katalog pracowników</h1>
        <p>Zaloguj się, aby otrzymać dostęp do kreatora katalogu.</p>
        <?php if ($message != '') : ?>
        <div class="message">
            <?php echo $message; ?>
        </div>
        <?php endif ;?>
        
        <?php echo Form::open('user/login',array('class' => 'well form-inline')); ?>
        
        <?php echo Form::label('username', 'Nazwa użytkownika'); ?>
        <?php echo Form::input('username', HTML::chars(Arr::get($_POST, 'username'))); ?>
        
        <?php echo Form::label('password', 'Hasło'); ?>
        <?php echo Form::password('password'); ?>

        <?php echo Form::submit('login', 'Login', array('class' => 'btn btn-success')); ?>

        <?php echo Form::close(); ?>
    </div>
</div>