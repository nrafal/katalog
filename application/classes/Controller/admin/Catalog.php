<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Catalog extends Controller_Template {

    public function before()
    {
        parent::before() ;

        if($this->auto_render) {
            $this->template->styles = array() ;
            $this->template->scripts = array() ;
        }
    }
    
    public function after()
    {
        if($this->auto_render)
        {
            $styles = array('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css') ;
            $scripts = array('http://ajax.googleapis.com/ajax/libs/jquery/1.3.2.js') ;
            $scripts = array($scripts, (array)'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js') ;
            
            $this->template->styles = array_reverse(array_merge($this->template->styles, $styles)) ;
         }
        
        parent::after() ;
    }
    
    public function action_index()
    {
       if (Auth::instance()->logged_in()) {
           $results = DB::select('id','name')->from('catalogs')->execute() ;
           $results = $results->as_array() ;
           if (empty($results)) {
               $info = 'Brak katalogów.' ;
               $this->template->content = View::factory('panel/catalogs')
                    ->bind('results', $info) ;
           }
           else {
               $this->template->content = View::factory('panel/catalogs')
                    ->bind('results', $results) ;
           }
       } else {
           $this->redirect('user/login') ;
       }
    }
    
    public function action_add()
    {
        if (Auth::instance()->logged_in()) {
           if ($this->request->method() == HTTP_Request::POST) {
               $post = $this->request->post() ;
               $insert = DB::insert('catalogs')
                         ->columns(array('name'))
                         ->values(array($post['name'])) ;
               list($insert_id, $affected_rows) = $insert->execute();
               
               if (!$insert_id || !$affected_rows) {
                   $message = 'Katalog nie został utworzony!' ;
                   $this->template->content = View::factory('panel/catalog_add')
                        ->bind('message',$message) ;
               } else {
                   $message = 'Katalog został utworzony!' ;
                   $this->template->content = View::factory('panel/catalog_add')
                        ->bind('message',$message) ;
               }
           }
           else $this->template->content = View::factory('panel/catalog_add') ;
       } else {
           $this->redirect('user/login') ;
       }
    }

    public function action_edit()
    {
        if (Auth::instance()->logged_in()) {
           
           if ($this->request->method() == HTTP_Request::POST) {
               $post = $this->request->post() ;
               $update = DB::update('catalogs')
                         ->value('name',$post['name'])
                         ->where('id','=',$this->request->current()->param('id')) ;
               $affected = $update->execute() ;
               if (!$affected) {
                   $message = 'Katalog nie został zmieniony!' ;
                   $this->template->content = View::factory('panel/catalog_edit')
                        ->bind('message',$message) ;
               } else {
                   $message = 'Katalog został zmieniony!' ;
                   $result = DB::select('id','name')->from('catalogs')->where('id','=',$this->request->current()->param('id'))->as_object()->execute() ;
                   $this->template->content = View::factory('panel/catalog_edit')
                    ->bind('result',$result)
                    ->bind('message',$message) ;
               }
           }
           else {
               $result = DB::select('id','name')->from('catalogs')->where('id','=',$this->request->current()->param('id'))->as_object()->execute() ;
               $this->template->content = View::factory('panel/catalog_edit')
                    ->bind('result',$result) ;
           }
       } else {
           $this->redirect('user/login') ;
       }
    }

    public function action_delete() 
    {
        if (Auth::instance()->logged_in()) {
            
            $emp = new Controller_Admin_Employee($this->request,$this->response) ;
            $results = DB::select('id')->from('employees')->where('catalog_id','=',$this->request->current()->param('id'))->execute() ;
            $results = $results->as_array('id') ;

            foreach ($results as $result) {
                $emp->action_delete($result['id']) ;
            }
            
            $delete = DB::delete('catalogs')->where('id','=',$this->request->current()->param('id')) ;
            
            if (!($delete->execute())) {
                $this->redirect('admin/catalog') ;
            } else {
                $this->redirect('admin/catalog') ;
            }
        }
    }
}