<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Employee extends Controller_Template {

    public function before()
    {
        parent::before() ;

        if($this->auto_render) {
            $this->template->styles = array() ;
            $this->template->scripts = array() ;
        }
    }
    
    public function after()
    {
        if($this->auto_render)
        {
            $styles = array('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css') ;
            $scripts = array('http://ajax.googleapis.com/ajax/libs/jquery/1.3.2.js') ;
            $scripts = array($scripts, (array)'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js') ;
            
            $this->template->styles = array_reverse(array_merge($this->template->styles, $styles)) ;
         }
        
        parent::after() ;
    }
    
    public function action_index()
    {
       if (Auth::instance()->logged_in()) {
           $results = DB::select('name')->from('catalogs')->execute() ;
           $results = $results->as_array() ;
           if (empty($results)) {
               $info = 'Najpierw utwórz katalog.' ;
               $this->template->content = View::factory('panel/employees')
                    ->bind('results', $info) ;   
           }
           else {
               $results = DB::select('id','firstname','lastname')->from('employees')->execute() ;
               $results = $results->as_array() ;
               if (empty($results)) {
                   $info = 'Brak pracowników.' ;
                   $this->template->content = View::factory('panel/employees')
                        ->bind('results', $info) ;
               }
               else {
                   $this->template->content = View::factory('panel/employees')
                        ->bind('results', $results) ;
               }
           }
       } else {
           $this->redirect('user/login') ;
       }
    }
    
    public function action_add()
    {
        if (Auth::instance()->logged_in()) {
           
           $results = DB::select('name')->from('catalogs')->execute() ;
           $results = $results->as_array() ;
           if (empty($results)) {
               $info = 'Najpierw utwórz katalog.' ;
               $this->template->content = View::factory('panel/employees')
                    ->bind('results', $info) ;   
           } else {
           
               $catalogs = DB::select('id','name')->from('catalogs')->execute() ;
               $catalogs = $catalogs->as_array('id','name') ;
               
               if ($this->request->method() == HTTP_Request::POST) {
                   $post = $this->request->post() ;
     
                   $name = md5($post['firstname'] . $post['lastname'] . time() . rand(1000,9999)) ;
                   
                   $image_id = $this->image_uploader($_FILES['image'],$name) ;
    
                   if ($image_id != FALSE) {               
                       $insert = DB::insert('employees')
                                 ->columns(array('firstname','lastname','mail','catalog_id','image_id'))
                                 ->values(array($post['firstname'],$post['lastname'],$post['mail'],$post['catalog'],$image_id)) ;
                       list($insert_id, $affected_rows) = $insert->execute();
    
                       if (!$insert_id || !$affected_rows) {
                           $message = 'Pracownik nie został utworzony!' ;
                           $this->template->content = View::factory('panel/employee_add')
                                ->bind('catalogs',$catalogs) 
                                ->bind('message',$message) ;
                       } else {
                           $message = 'Pracownik został utworzony!' ;
                           $this->template->content = View::factory('panel/employee_add')
                                ->bind('catalogs',$catalogs) 
                                ->bind('message',$message) ;
                       }
                   } else {
                       
                       $message = 'Pracownik nie został utworzony!' ;
                           $this->template->content = View::factory('panel/employee_add')
                                ->bind('catalogs',$catalogs) 
                                ->bind('message',$message) ;
                   }
               }
               else {
                   $this->template->content = View::factory('panel/employee_add')
                        ->bind('catalogs',$catalogs) ;
               }
           }
       } else {
           $this->redirect('user/login') ;
       }
    }

    public function action_delete($id = null) 
    {
        if (Auth::instance()->logged_in()) {
            
            $image = null ;
            $delete = null ;
            
            if (!$id) {
                $image = DB::select('image_id')->from('employees')->where('id','=',$this->request->current()->param('id'))->execute() ;
            } else {
                $image = DB::select('image_id')->from('employees')->where('id','=',$id)->execute() ;
            }
            $image = $image->as_array() ;
            $image_id = $image[0]['image_id'] ; 

            $_image = DB::select('link')->from('images')->where('id','=',$image_id)->execute() ;
            $_image = $_image->as_array() ;
            $image_link = $_image[0]['link'] ;
            
            unlink (getcwd() . DIRECTORY_SEPARATOR . $image_link) ;
            
            DB::delete('images')->where('id','=', $image_id)->execute() ;
            
            if (!$id) {
            $delete = DB::delete('employees')->where('id','=',$this->request->current()->param('id')) ;
            } else {
                $delete = DB::delete('employees')->where('id','=',$id) ;
            }
            if (!$id) {
                if (!($delete->execute())) {
                    $this->redirect('admin/employee') ;
                } else {
                    $this->redirect('admin/employee') ;
                }
            } else {
                $delete->execute() ;
            }
        }
    }
    
    public function action_show() 
    {
        if (Auth::instance()->logged_in()) {
            $result = DB::select('employees.firstname','employees.lastname','employees.mail','catalogs.name','images.link')
                      ->from('employees')
                      ->join('catalogs')
                      ->on('employees.catalog_id','=','catalogs.id')
                      ->join('images')
                      ->on('employees.image_id','=','images.id')
                      ->where('employees.id','=',$this->request->current()->param('id'))
                      ->execute() ;
            
            $result = $result->as_array() ;
            
            if (empty($result)) {
                $this->redirect('admin/employee') ; 
            } else {
                $this->template->content = View::factory('panel/employee_show')
                        ->bind('result',$result) ;
            }
        }
    }
    
    private function image_uploader ($image, $name) 
    {
        if ($image['type'] == 'image/jpeg' || $image['type'] == 'image/png')
        {
            if ($image['size'] < 2097152) {
                list($width, $height) = getimagesize($image['tmp_name']) ;
                $new_width = 0 ;
                $new_height = 0 ;
                
                if (($width > $height) || ($width == $height)) {
                    if ($width > 300) {
                        $percent = (float)(300/$width) ;
                        $new_width = (int)($width * $percent) ;
                        $new_height = (int)($height * $percent) ;
                    }
                } else {
                    if ($height > 300) {
                        $percent = (float)(300/$height) ;
                        $new_width = (int)($width * $percent) ;
                        $new_height = (int)($height * $percent) ;
                    }
                }
                
                $src = null ;
                
                if ($image['type'] == 'image/jpeg') $src = imagecreatefromjpeg($image['tmp_name']) ;
                if ($image['type'] == 'image/png') $src = imagecreatefrompng($image['tmp_name']) ;
                
                $im = imagecreatetruecolor($new_width, $new_height) ;
                imagecopyresampled($im, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height) ;                
                
                $link = 'images' . DIRECTORY_SEPARATOR . $name . '.jpg' ;
                
                if(imagejpeg($im, getcwd() . DIRECTORY_SEPARATOR . $link)) {
                    $insert = DB::insert('images')
                              ->columns(array('link'))
                              ->values(array($link));
                    list($insert_id, $affected_rows) = $insert->execute();      
                    
                    if (!$insert_id || !$affected_rows) {
                        return FALSE ;
                    }
                    else {
                        return $insert_id ;
                    }  
                }
                    
            }
        }
    }
}