<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Panel extends Controller_Template {

    public function before()
    {
        parent::before() ;

        if($this->auto_render) {
            $this->template->styles = array() ;
            $this->template->scripts = array() ;
        }
    }
    
    public function after()
    {
        if($this->auto_render)
        {
            $styles = array('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css') ;
            $scripts = array('http://ajax.googleapis.com/ajax/libs/jquery/1.3.2.js') ;
            $scripts = array($scripts, (array)'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js') ;
            
            $this->template->styles = array_reverse(array_merge($this->template->styles, $styles)) ;
         }
        
        parent::after() ;
    }
    
    public function action_index()
    {
       if (Auth::instance()->logged_in()) {
           $results = DB::select('name')->from('catalogs')->execute() ;
           $results = $results->as_array() ;
           if (empty($results)) {
               $info = 'Brak katalogów. Wejdź w menu "Katalogi" i utwórz nowy.' ;
               $this->template->content = View::factory('panel/index')
                    ->bind('info', $info) ;
           }
           else {
               $info = 'Zarządzanie katalogami: Menu->Katalogi, <br/> Zarządzanie pracownikami: Menu->Pracownicy' ;
               $this->template->content = View::factory('panel/index')
                    ->bind('info', $info) ;
           }
       } else {
           $this->redirect('user/login') ;
       }
    }
}