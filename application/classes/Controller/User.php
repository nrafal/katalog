<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Template {

    public function before()
    {
        parent::before() ;

        if($this->auto_render) {
            $this->template->styles = array() ;
            $this->template->scripts = array() ;
        }
    }
    
    public function after()
    {
        if($this->auto_render)
        {
            $styles = array('media/css/bootstrap.css') ;
            $styles = array_merge($styles, (array)'media/css/bootstrap.min.css') ;
            
            $scripts = array('http://ajax.googleapis.com/ajax/libs/jquery/1.3.2.js') ;
            $scripts = array_merge((array)'media/js/bootstrap.js', $scripts) ;
            
            $this->template->styles = array_reverse(array_merge($this->template->styles, $styles)) ;
            $this->template->scripts = array_reverse(array_merge($this->template->scripts, $scripts)) ;
         }
        
        parent::after() ;
    }
    
    public function action_index()
    {
        if (Auth::instance()->logged_in()) {
            $this->redirect('admin') ;
        } else {
            $this->redirect('user/login') ;
        }
    }
    
    public function action_login() 
    {
        if ($this->request->method() == HTTP_Request::POST) {
            $post = $this->request->post() ;
            $success = Auth::instance()->login($post['username'], $post['password']) ;
             
            if ($success) {
                $this->redirect('admin') ;
            }
            else {
                $message = 'Błędny login i/lub hasło!' ;
                $this->template->content = View::factory('user/login')
                ->bind('message', $message) ;
            }
        }
        else {
            $message = '' ;
            $this->template->content = View::factory('user/login')
            ->bind('message', $message) ;
        }
    }
    
    public function action_logout() 
    {
        Auth::instance()->logout() ;
        $this->redirect('user/login') ;
    }

}